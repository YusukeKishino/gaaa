# frozen_string_literal: true

require 'bigdecimal'

module Gaaa
  class Processor
    attr_accessor :genoms

    def initialize(options: {})
      @options = options
      @genom_length = options[:genom_length] || 100
      @max_genom_list = options[:max_genom_list] || 100
      @select_genom = options[:select_genom] || 20
      @individual_mutation = options[:individual_mutation] || 0.1
      @genom_mutation = options[:genom_mutation] || 0.1
      @max_generation = options[:max_generation] || 100
    end

    def run(&block)
      @genoms = []
      @max_genom_list.times { |_| @genoms.push(create_genom(@genom_length)) }

      @max_generation.times do |generation|
        @genoms.each do |genom|
          yield(genom)
        end

        elites = select
        progenies = elites.each_with_index.map { |genom, i| crossover(genom, elites[i-1] ) }.flatten
        next_generation = generate_next_generation(@genoms, progenies)
        next_generation = mutate(next_generation)

        print_log(@genoms, generation)

        @genoms = next_generation
      end
      @genoms.sort { |genom| genom.evaluation }.last
    end

    private

    def create_genom(length)
      genom_list = length.times.map { [0, 1].sample }
      Genom.new(genom_list: genom_list, evaluation: 0)
    end

    def select
      @genoms.sort { |a, b| b.evaluation <=> a.evaluation }.slice(0, @select_genom)
    end

    def crossover(genom1, genom2)
      cross_first = (0...@genom_length).to_a.sample
      cross_second = (cross_first...@genom_length).to_a.sample

      first = genom1.genom_list
      second = genom2.genom_list
      progeny_first = first.slice(0...cross_first) + second.slice(cross_first...cross_second) + first.slice(cross_second...@genom_length)
      progeny_second = second.slice(0...cross_first) + first.slice(cross_first...cross_second) + second.slice(cross_second...@genom_length)

      [Genom.new(genom_list: progeny_first, evaluation: 0), Genom.new(genom_list: progeny_second, evaluation: 0)]
    end

    # rubocop:disable Style/SymbolProc
    def generate_next_generation(genoms, progenies)
      next_generation = genoms.sort { |genom| genom.evaluation }.slice(progenies.length - 1...genoms.length - 1)
      next_generation + progenies
    end

    def mutate(genoms)
      genoms.each do |genom|
        next unless @individual_mutation > (BigDecimal(rand(100).to_s) / BigDecimal('100'))

        genom.genom_list.map! do |n|
          if @genom_mutation > (BigDecimal(rand(100).to_s) / BigDecimal('100'))
            [0, 1].sample
          else
            n
          end
        end
      end
    end

    def print_log(genoms, generation)
      evaluates = genoms.map(&:evaluation)
      puts "----第#{generation}世代の結果----"
      puts " Min: #{evaluates.min}"
      puts " Max: #{evaluates.max}"
      puts " Avg: #{evaluates.sum / evaluates.length}"
    end
  end
end
