# frozen_string_literal: true

module Gaaa
  class Configuration
    attr_accessor :genom_length, :max_genom_list, :select_genom,
                  :individual_mutation, :genom_mutation, :max_generation
    def initialize
      genom_length = 100
      max_genom_list = 100
      select_genom = 20
      individual_mutation = 0.1
      genom_mutation = 0.1
      max_generation = 40
    end
  end
end
