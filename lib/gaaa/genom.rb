# frozen_string_literal: true

module Gaaa
  class Genom
    attr_accessor :genom_list, :evaluation

    def initialize(genom_list: [], evaluation: 0)
      @genom_list = genom_list
      @evaluation = evaluation
    end

    def length
      @genom_list.length
    end
  end
end
