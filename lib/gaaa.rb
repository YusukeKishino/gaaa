# frozen_string_literal: true

require 'gaaa/version'

require 'gaaa/configuration'
require 'gaaa/processor'
require 'gaaa/genom'

module Gaaa
  class Error < StandardError; end
  # Your code goes here...

  class << self
    def configure
      yield configuration
    end
  end

  def configuration
    @configuration ||= Gaaa::Configuration.new
  end
end
