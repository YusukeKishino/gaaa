# frozen_string_literal: true

FactoryBot.define do
  factory :genom, class: Gaaa::Genom do
    genom_list { [0, 0, 0, 0, 0] }
    evaluation { 0 }
  end
end
