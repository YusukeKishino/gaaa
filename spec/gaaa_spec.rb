# frozen_string_literal: true

RSpec.describe Gaaa do
  it 'has a version number' do
    expect(Gaaa::VERSION).not_to be nil
  end
end
