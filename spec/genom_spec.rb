# frozen_string_literal: true

RSpec.describe Gaaa::Genom do
  describe '#new' do
    subject { described_class.new(genom_list: genom_list, evaluation: evaluation) }

    let(:genom_list) { [0, 1] }
    let(:evaluation) { 0 }

    it { is_expected.to be_instance_of(described_class) }
  end

  describe '#length' do
    subject { genom.length }

    let(:genom) { build(:genom, genom_list: genom_list) }
    let(:genom_list) { [0, 1] }

    it { is_expected.to eq 2 }
  end
end
