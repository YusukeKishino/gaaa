# frozen_string_literal: true

RSpec.describe Gaaa::Processor do
  describe '#create_genom' do
    subject { processor.send(:create_genom, length) }

    let(:processor) { described_class.new }
    let(:length) { 10 }

    it { is_expected.to be_instance_of(Gaaa::Genom) }
    it { expect(subject.length).to eq(length) }
  end

  describe '#select' do
    subject { processor.send(:select) }

    let(:options) do
      {
        select_genom: 2,
      }
    end
    let(:processor) { described_class.new(options: options) }
    let(:genoms) do
      [
        build(:genom, evaluation: 1),
        build(:genom, evaluation: 0),
        build(:genom, evaluation: 3),
      ]
    end

    before do
      processor.genoms = genoms
    end

    it { expect(subject.length).to eq 2 }
    it { expect(subject.first.evaluation).to eq 3 }
  end

  describe '#crossover' do
    subject { processor.send(:crossover, genom1, genom2) }

    let(:processor) { described_class.new(options: { genom_length: genom1.length }) }
    let(:genom1) { build(:genom) }
    let(:genom2) { build(:genom) }

    it { is_expected.to be_instance_of(Array) }
    it { expect(subject.first).to be_instance_of(Gaaa::Genom) }
    it { expect(subject.first.length).to eq genom1.length }
  end

  describe '#generate_next_generation' do
    subject { processor.send(:generate_next_generation, genoms, progenies) }

    let(:processor) { described_class.new }
    let(:genoms) { [genom1, genom2, genom3, genom4, genom5] }
    let(:progenies) { [genom6, genom7] }
    let(:genom1) { build(:genom, evaluation: 1) }
    let(:genom2) { build(:genom, evaluation: 2) }
    let(:genom3) { build(:genom, evaluation: 4) }
    let(:genom4) { build(:genom, evaluation: 3) }
    let(:genom5) { build(:genom, evaluation: 4) }
    let(:genom6) { build(:genom) }
    let(:genom7) { build(:genom) }

    it { is_expected.to be_instance_of(Array) }
    it { expect(subject.first).to be_instance_of(Gaaa::Genom) }
    it { expect(subject.length).to eq genoms.length }
  end

  describe '#mutate' do
    subject { processor.send(:mutate, genoms) }

    let(:processor) { described_class.new }
    let(:genoms) { [genom] }
    let(:genom) { build(:genom, genom_list: genom_list) }
    let(:genom_list) { [0, 1, 1] }

    it { is_expected.to be_instance_of(Array) }
    it { expect(subject.first).to be_instance_of(Gaaa::Genom) }
    it { expect(subject.first.length).to eq genom.length }
  end
end
